/*
 * gameThread.h
 */
#ifndef GAME_THREAD_H
#define GAME_THREAD_H

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <poll.h>
#include <unistd.h>
#include <stdlib.h>
#include "main.h"

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define POLL_TIMEOUT (3 * 1000) /* 3 seconds */
#define MAX_BUF 64

void gameThread(void *args);

#endif
