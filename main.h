/*
 * main.h
 */
#ifndef MAIN_H
#define MAIN_H

#include "timeutil.h"
#include "gpioInterface.h"
#include "string.h"
#include "gameThread.h"
#include <stdint.h>
#include <pthread.h>

	typedef struct timespec timespec;

	typedef struct game_player {
		uint32_t inputPin;
		char name[128];
		uint32_t outputLED;
		timespec elapsedLatency;
		uint8_t rounds;
	} game_player;
	
#endif
