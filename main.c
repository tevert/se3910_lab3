/*
 * main.c
 */
#include "main.h"

/*
 * Main function
 *
 * Runs a reaction measurement game between two players, multithreaded. 
 * Handles the arguments, gauges the winner, and annouces the results. 
 */
int main(int argc, char *argv[]) {
	
	// Declare all variables
	game_player player1;
	game_player player2;
	pthread_t thread1;
	pthread_t thread2;

	// Check for the correct # of arguments
	if (argc != 4){
		// We print argv[0] assuming it is the program name
        	printf("usage: %s <player1 name> <player2 name> <number of rounds>\n", argv[0]);
	} else {
		// Print welcome message
		printf("***********************************\n");
		printf("* Welcome %s and %s \n", argv[1], argv[2]);
		printf("* %s will be playing on switch 1 \n", argv[1]);
		printf("* %s will be playing on switch 2 \n", argv[2]);
		printf("***********************************\n");
		
		// Initialize the gameplayer structures with the player names and IO pins, and set the time to be 0
		player1.inputPin = 65;
		player1.outputLED = 26;
		player1.rounds = atoi(argv[3]);
		strcpy(player1.name, argv[1]);

		player2.inputPin = 37;
		player2.outputLED = 46;
		player2.rounds = atoi(argv[3]);
		strcpy(player2.name, argv[2]);

		pthread_create(&thread1, NULL, (void *)gameThread, &player1);		
		pthread_create(&thread2, NULL, (void *)gameThread, &player2);
	
		pthread_join(thread1, NULL);
		pthread_join(thread2, NULL);
		
		printf("***********************************\n");
		printf("******THIS GAME IS HEREBY OVER*****");
		printf("***********************************\n");
		printf("                  {}\n");
		printf("  ,   A           {}\n");
		printf(" / \\,| ,         .--.\n");
		printf("|  =|= >        /.--.\\\n");
		printf(" \\ /`| `        |====|\n");
		printf("  `   |         |`::`|\n");
		printf("      |     .-;`\\..../`;_.-^-._\n");
		printf("     /\\\\/  /  |...::..|`   :   `|\n");
		printf("     |:'\\ |   /'''::''|   .:.   |\n");
		printf("      \\ /\\;-,/|   ::  |.........|\n");
		printf("      |\\ <` >  >._::_.| ':::::' |\n");
		printf("      | `""`_/  ^^/>/>  |   ':'   |\n");
		printf("      |       |       \\    :    /\n");
		printf("      |       |        \\   :   /\n");
		printf("      |       |___/\\___|`-.:.-`\n");
		printf("      |        \\_ || _/    `\n");
		printf("      |        <_ >< _>\n");
		printf("      |        |  ||  |\n");
		printf("      |        |  ||  |\n");
		printf("      |       _\\.:||:./_\n");
		printf("      |      /____/\\____\\\n");
		printf("\n\n");
		
		printf("Player 1 Latency: %d ms\n", timespectoms(&(player1.elapsedLatency)));
		printf("Player 2 Latency: %d ms\n\n", timespectoms(&(player2.elapsedLatency)));

		if (timespectoms(&(player1.elapsedLatency)) < timespectoms(&(player2.elapsedLatency)))
		{
			printf("***********************************\n");
			printf("	%s Wins !!!!!!!!\n", argv[1]);
			printf("***********************************\n\n");
			printf("   /:""|\n");
          		printf("  |:`66|_ \n");
          		printf(" C`    _) \n");
           		printf("   \\ ._|\n");
            		printf("    ) /\n");
           		printf("   /`\\\n");
          		printf("  || |Y|\n");
          		printf("  || |#|\n");
          		printf("  || |#|\n");
          		printf("  || |#|\n");
          		printf("  :| |=:\n");
          		printf("  ||_|,|\n");
         		printf("  \\)))||\n");
       			printf("|~~~`-`~~~|\n");
       			printf("|         |\n");
       			printf("|_________|\n");
       			printf("|_________|\n");
           		printf("   | ||\n");
           		printf("   |_||__ \n");
           		printf("   (____))\n");
		} else {
			printf("***********************************\n");
			printf("	%s Wins !!!!!!!!\n", argv[2]);
			printf("***********************************\n\n");
			printf("                 ,#####,\n");
			printf("                 #_   _#\n");
 			printf("                 |e` `e|\n");
 			printf("                 |  u  |\n");
 			printf("                 \\  =  /\n");
 			printf("                 |\\___/|\n");
 			printf("        ___ ____/:     :\\____ ___\n");
 			printf("      .'   `.-===-\\   /-===-.`   '.\n");
 			printf("     /      .-""""'-.-'""""-.      \\\n");
 			printf("    /'             =:=             '\\\n");
 			printf("  .'  ' .:    o   -=:=-   o    :. '  `.\n");
 			printf("  (.'   /'. '-.....-'-.....-' .'\\   '.)\n");
 			printf("  /' ._/   '.     --:--     .'   \\_. '\\\n");
 			printf(" |  .'|      '.  ---:---  .'      |'.  |\n");
 			printf(" |  : |       |  ---:---  |       | :  |\n");
 			printf("  \\ : |       |_____._____|       | : /\n");
 			printf("  /   (       |----|------|       )   \\ \n");
 			printf(" /... .|      |    |      |      |. ...\\\n");
 			printf("|::::/''    '-.,__/` `\\__..-'\\\n");
 			printf("          ;      /     \\      ;\n");
 			printf("          :     /       \\     |\n");
 			printf("          |    /         \\.   |\n");
 			printf("          |`../           |  ,/\n");
 			printf("          ( _ )           |  _)\n");
 			printf("          |   |           |   |\n");
 			printf("          |___|           \\___|\n");
 			printf("          :===|            |==|\n");
 			printf("           \\  /           |__|\n");
 			printf("           /\\/\\          /'''.__\n");
 			printf("           |oo|          \\__.//___\n");
 			printf("           |==|\n");
 			printf("           \\__/\n");
		}

		printf("   _____                 _ _ \n");               
  		printf("  / ____|               | | |\n");               
 		printf(" | |  __  ___   ___   __| | |__  _   _  ___ \n");
 		printf(" | | |_ |/ _ \\ / _ \\ / _` | '_ \\| | | |/ _ \\\n");
 		printf(" | |__| | (_) | (_) | (_| | |_) | |_| |  __/\n");
  		printf("  \\_____|\\___/ \\___/ \\__,_|_.__/ \\__, |\\___|\n");
                printf("		                  __/ |\n");    
                printf("		                 |___/\n");    

	}
	// Do we want to return error conditions elsewhere?
	return 0;
}
