/*
 * gameThread.c
 */
#include "gameThread.h"
/*
 * Function: gameThread
 * Arguments: void *
 * Returns: Nothing
 *
 * This function runs a game process that turns on a LED and measures the 
 * response time for a pushbutton response. The void pointer argument is
 * expected to be a pointer to a game_player type. On return, this function
 * will have populated the game_player's latency measurement. 
 */
void gameThread(void *args){
	// Initialize variables
	int i, j, btn_fd, rc;
	long int random;
	char keyboardKey;
	timespec start, end, diff;	
	end.tv_nsec = 0;
	game_player *player = args;
	struct pollfd fdset[2];
	char buf[MAX_BUF];

	// Zero out the initial latency
	player->elapsedLatency.tv_sec = 0;
	player->elapsedLatency.tv_nsec = 0;
	
	// Initialize the input push button
	gpio_export(player->inputPin);
	gpio_set_dir(player->inputPin, 0);
	gpio_set_edge(player->inputPin, 2);
	btn_fd = gpio_fd_open(player->inputPin); 

	// Loop for the number of game rounds
	for (i = 0; i < player->rounds; i++) {
		// Random wait
		random = rand();
		random = (random * 10.0 * 1000000) / RAND_MAX;
		usleep(random);

		// Turn on the LED
		gpio_export(player->outputLED);
		gpio_set_dir(player->outputLED, 1);
		gpio_fd_open(player->outputLED);
		gpio_set_value(player->outputLED, 1);
		
		// Get the start time
		clock_gettime(CLOCK_REALTIME, &start);

		// Give the user 10 seconds to respond
		for (j = 0; j < 10; j++) {
			// Listen for an interuppt (from pushbutton or STDIN)
			memset((void*)fdset, 0, sizeof(fdset));
			fdset[0].fd = STDIN_FILENO;
			fdset[0].events = POLLIN;

			fdset[1].fd = btn_fd;
			fdset[1].events = POLLPRI;
			fdset[1].revents = 0;

			rc = poll(fdset, 2, 5000);

			// Decide if we timed out or had an interuppt
			if (rc < 0) {
				printf("\npoll() failed! Something is going on. Aborting program!\n");
				return;
			} else if (rc == 0) {
				// Poll timed out
				printf("Come on %s! Get your butt in the game!\r\n", player->name);
			} else if (fdset[1].revents & POLLPRI) {
				// Button Pressed
				lseek(fdset[1].fd, 0, SEEK_SET);
				read(fdset[1].fd, buf, MAX_BUF);
				break;
			} else if (fdset[0].revents & POLLPRI) {
				// Assume our players have different first letters, for test purposes
				(void)read(fdset[0].fd, &keyboardKey, 1);
				if (keyboardKey == (player->name)[0]) {
					break;
				}
			} else {
				printf("WARNING: Unknown program state.");
			}
			fflush(stdout);
		}
		// Grab the end time
		clock_gettime(CLOCK_REALTIME, &end);
		// Calculate and print the results
		timeval_subtract(&diff, &end, &start);
		timeval_add(&(player->elapsedLatency), &(player->elapsedLatency), &diff);
		printf("Player %s took %d ms\n", player->name, timespectoms(&diff));
		printf("Total latency so far: %d ms\n", timespectoms(&(player->elapsedLatency)));

		// Close the LED
		gpio_set_value(player->outputLED, 0);
		sleep(2);
	}

	// Close the pushbutton
	gpio_fd_close(btn_fd);
	gpio_unexport(player->outputLED);
	gpio_unexport(player->inputPin);
}


